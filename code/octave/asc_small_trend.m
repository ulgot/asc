%%%
%    -- Funkcja asc_small_trend(x, d)
%         szacuje wektory trendu, sezonowości oraz reszt
%         danych wykazujących sezonowość metodą małego trendu
%
%         x - wektor danych (szereg czasowy)
%         d - okresowość danych
%
%         Zwraca
%         m_hat - wektor średniej z wartości szeregu {x} w danym okresie
%                 length(m_hat) = length(x) / d
%         s_hat - średni wektor sezonowości (jeden okres)
%         Y     - wektor reszt
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: March 2010

%
%       TODO:
%             funkcja powinna sama znajdować okres zmian
%             => asc_small_trend(x)
%

function [m_hat, s_hat, Y] = asc_small_trend(x, d)

  if ( isempty (x) || (!isvector(x)) ) 
    error ("asc_diff: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif
  
  if ( d < 1 )
    error("d > 0, podałeś d = %d", d);
    return;
  endif  
  
  ilosc_okresow = floor(length(x) / d);
  
  %%%
  % średnia na okres
  m_hat = zeros(1,ilosc_okresow);
  for j = 1 : ilosc_okresow
    for k = 1 : d
      m_hat(j) += x((j-1)*d + k);
    endfor
  endfor
  m_hat /= d;
  
  %%%
  % sezonowość
  s_hat = zeros(1,d);
  for k = 1 : d
    for j = 1 : ilosc_okresow
       s_hat(k) += x((j-1)*d + k) - m_hat(j);
    endfor
  endfor
  s_hat /= ilosc_okresow;
  
  %%%
  % wektor reszt
  for j = 1 : ilosc_okresow
    for k = 1 : d
      Y((j-1)*d + k) = x((j-1)*d + k) - m_hat(j) - s_hat(k);
    endfor
  endfor
  
endfunction
