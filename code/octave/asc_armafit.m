%%%
%    -- Funkcja [phi, theta, v] = asc_armafit(x, p, q)
%         dopasowywuje proces ARMA(p,q) do danego szeregu
%         czasowego x
%
%         przyjmuje jako agrumenty
%         x - stacjonarny szereg czasowy (niepusty wektor danych)
%         p - rząd wielomianu AR, p >= 0
%         q - rząd wielomianu MA, q >= 0
%
%         Ten program wymaga dowolnego programu
%         obliczającego wstępne wartości 'theta' 
%         np: algorytmu innowacyjnego zaimplementowanego w
%         asc_innovation(x,q)
%
%         Zwraca
%         phi   - wektor parametrów AR(p)
%         theta - wektor parametrów MA(q)
%         v     - wariancję procesu ARMA(p,q)
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: Mai 2010

%%%
% this function requires any software that 
% returns the pre-calculated set of 'theta' vector
% e.g. asc_innovation(x,q)
%%%

function [phi, theta, v] = asc_armafit(x, p, q)


  if ( isempty (x) || (!isvector(x)) ) 
    error ("armafit: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif

  %dlugosc wektora
  lx = length(x);

  if ( !isnumeric(p) || p < 0 || !isnumeric(p) || q < 0)
    error ("armafit: drugi i trzeci argument musi byc liczba naturalna p,q >= 0\n");
    return;
  endif

  %%%
  % wektoryzacja
  v     = [];
  phi   = [];
  theta = [];

  %%%
  % algorytm dla losowych procesów ARMA(p,q), o średniej zerowej
  %
  
  %%%
  % wstępne obliczenie parametrów theta
  % za pomoca algorytmu innowacyjnego
  % |NB:
  % |   w książce Brockwella jest błąd we wzorze 8.4.4,
  % |   ma być Theta_m,q+p-2 a nie Theta_m,q+p
  % |
  [preTheta, v] = asc_innovation(x,p+q);
  
  preTheta = preTheta(p+q,:);
%  v        = v(1);

  MpreTheta = [];
  for i=1:p
    for j=1:p
      index = q + j - i;
      if index < 1
	      localPreTheta = 0;
      else
	      localPreTheta = preTheta(index);
      endif
      MpreTheta(i,j) = localPreTheta;
    end
  end
  %%%%
  % spr. tymczasowej macierzy Theta
  % MpreTheta

  %%%
  % parametry AR(p)
  % Brockwell 8.4.4
  phi = inv(MpreTheta) * preTheta(q+1:q+p)';

  %%%
  % ponownie szacowane parametry MA(q)
  % teoria np: Brockwell 8.5.5
  suma = [];
  for j = 1:q
    suma(j) = 0;
    for i = 1:min(j,p)
      suma(j) += phi(i) * preTheta(j-i+1);
    end
  end
  theta = preTheta(1:q) - suma;

  %%%
  % oszacowanie
  phi   = phi;
  theta = theta';
  v     = v(p+q);

endfunction
