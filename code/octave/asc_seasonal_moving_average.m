%%%
%    -- Funkcja [m_hat, s_hat, d, Y] = asc_seasonal_moving_average(x [, d = -1, stopien_MA = 17])
%         szacuje waktory trendu, sezonowości oraz reszt
%         danych wykazujących sezonowość metodą średniej
%         kroczącej
%
%         x           - wektor danych (szereg czasowy)
%         d           - opcjonalnie: okresowość danych, jezeli nie podane
%                       program sam stara się oszacować okresowość [= -1]
%         stopien_MA  - opcjonalnie, ilość punktów dla średniej 
%                       kroczącej [= 17]
%
%         zwraca
%         m_hat - wstępny wektor trendu
%         s_hat - średnią okresowość
%         d     - wektor trendu
%         Y     - wektor reszt
%         
%%%

## This file is part of ASC project
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: March 2010

%
%       TODO:
%             funkcja powinna sama znajdować okres zmian
%             => asc_asc_seasonal_moving_average(x, d = -1)
%

function [m_hat, s_hat, d, Y] = asc_seasonal_moving_average(x, d = -1, stopien_MA = 17)

  if ( isempty (x) || (!isvector(x)) ) 
    error ("asc_seasonalMA: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif
  
  if ( d == -1 )
    error("Jeszcze nie zakodowane...");
  elseif (d == 0)
    error("asc_seasonalMA: d > 0, podałeś d = %d", d);
    return;
  endif  
  
  length_x = length(x);
  ilosc_okresow = floor(length_x / d);
  
  %%%
  % wstępne wygładzanie danych
  q = floor(d / 2);
  switch (mod(d, 2))
    case 0  % even
      for i = q + 1 : length_x - q
        m_hat(i - q) = sum(x(i - q:i + q)) - 0.5 * (x(i - q) + x(i + q));
      endfor
      m_hat /= d;
    case 1  % odd
      m_hat = asc_moving_average(x, d);
  endswitch

  %%%
  % obliczanie sezonowości
  % (1) średnia odchyleń
  for k = 1 : d
    w(k) = mean(x(q + k: d :length_x - q - 1) - m_hat(1 + k: d :length(m_hat)));
  endfor
  
  % (2) średni okres zmian
  s_hat = w - mean(w);

  % (3) sezonowość
  s_hat_long = repmat(s_hat, 1, ilosc_okresow - 1);  
  
  %%%
  % dane bez sezonowości
  d = x(q + 1:length_x - q - 1) - s_hat_long;
  
  %%%
  % reszty 
  % (trend obliczany jest prostą średnią kroczącą)
  floor_stopien_MA_pol = floor(stopien_MA / 2);
  Y = x(q + 1 + floor_stopien_MA_pol:length_x - q - 1 - floor_stopien_MA_pol) - asc_moving_average(d, stopien_MA) - s_hat_long(1 + floor_stopien_MA_pol:length(s_hat_long) - floor_stopien_MA_pol);
  
endfunction
