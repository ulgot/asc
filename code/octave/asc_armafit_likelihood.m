%%%
%    -- Funkcja [phi, theta, v, S] = asc_armafit_likelihood(x, p, q, method='likeli')
%         dopasowywuje proces ARMA(p,q) do danego szeregu
%         czasowego x optymalizując p i q metodami
%         a) maksymalnej wiarygodności
%         b) najmniejszych kwadratów estymatorów
%
%         przyjmuje jako agrumenty
%         x - stacjonarny szereg czasowy (niepusty wektor danych)
%         p - rząd wielomianu AR, p >= 0
%         q - rząd wielomianu MA, q >= 0
%         method - metoda optymalizacji parametrów modelu
%                  [likeli, square]
%
%         Ten program wymaga dowolnego programu
%         obliczającego wstępne wartości '\phi, \theta i v' 
%         np: algorytmu zaimplementowanego w
%         asc_armafit(x,p,q)
%
%         Zwraca
%         phi   - wektor parametrów AR(p)
%         theta - wektor parametrów MA(q)
%         v     - wariancję procesu ARMA(p,q)
%         S     - wybrana statystyka do optymalizacji
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: Dec 2010

function [phi, theta, v, stat] = asc_armafit_likelihood(x, P, Q, method='likeli')

    if ( isempty (x) || (!isvector(x)) ) 
    error ("armafit: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif

  %dlugosc wektora
  lx = length(x);

  if ( !isnumeric(P) || P < 0 || !isnumeric(Q) || Q < 0)
    error ("armafit: drugi i trzeci argument musi byc liczba naturalna p,q >= 0\n");
    return;
  endif

  v     = [];
  phi   = [];
  theta = [];
  
  S_params = zeros(P * Q, P + Q);
  S        = zeros(P*Q,1);
  indexes  = zeros(P*Q,2);
  for p = 1 : P
    for q = 1 : Q
      i = (p - 1) * Q + q;
      
      indexes(i,1:2) = [p,q];
      
      [phi_tmp, theta_tmp, v_tmp] = asc_armafit(x, p, q);
      S_params(i, 1 : p + q) = [phi_tmp', theta_tmp'];
      
      [xbar, r] = asc_one_step_predictors(x, phi_tmp, theta_tmp);
      S(i) = sum(power(x(2:end)' - xbar(2:end),2)./r(1:end-1));      
    endfor
  endfor
  
  if method == "square"   
    stat = S;
  else
    stat = log(S / lx) + sum(log(r)) / lx;    
  endif

  [S_value, S_index] = min(stat);  
  v     = min(S) / lx;
  phi   = S_params(S_index, 1 : indexes(S_index, 1));
  theta = S_params(S_index, indexes(S_index,1) + 1 : indexes(S_index, 1) + indexes(S_index, 2));

endfunction
