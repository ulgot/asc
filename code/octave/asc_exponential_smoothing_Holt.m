%%%
%    -- Funkcja [F, S] = asc_exponential_smoothing_Holt(x, a = 0.5, b = 0.5)
%         wygładza szereg czasowy
%
%         przyjmuje jako agrumenty
%         x - szereg czasowy (niepusty wektor danych)
%         a - parametr wygładzania wykładniczego
%             0 < a < 1 [= 0.5]
%         b - parametr wygładzania wykładniczego
%             0 < b < 1 [= 0.5]
%         initial - wybór parametrów początkowych dla F i S
%             vec: F(1) = x(1), S(1) = x(2) - x(1)
%             lin : F(1) - współczynnik wolny liniowego dopasowania do x
%                   S(1) - współczynnik kierunkowy liniowego dopasowania do x
%
%         Zwraca
%         F - wygładzona wartość zmiennej {x}
%         S - wygładzona wartość trendu
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: Dec 2010

function [F, S] = asc_exponential_smoothing_Holt(x, a = 0.5, b = 0.5, initial = "vec")
  
  length_x = length(x);
  
  if ( isempty (x) || (!isvector(x)) || length_x < 2) 
    error ("asc_exponential_smoothing_Holt: \
    pierwszy agrument musi byc niepustym wektorem");
    return;
  endif
  
  if (a < 0 || a > 1)
    error("asc_exponential_smoothing_Holt: \
    a=%d, Podaj 0 < a < 1\n",a);
    return;
  endif

  if (b < 0 || b > 1)
    error("asc_exponential_smoothing_Holt: \
    b=%d, Podaj 0 < b < 1\n",b);
    return;
  endif
  
  if initial == "vec"
    F(1) = x(1);
    S(1) = x(2) - x(1);
  elseif initial == "lin"
    [p1, p2, p3] = polyfit([1 : length_x]', x, 1);
    F(1) = p1(1);
    S(1) = p1(2);
  else
    error("expecting 'vec' or 'lin' for initial values, '%s' given", initial);
    return;
  endif
  
  for t = 2 : length_x
    F(t) = a * x(t) + (1 - a) * (F(t - 1) + S(t - 1));
    S(t) = b * (F(t) - F(t - 1)) + (1 - b) * S(t - 1);
  endfor
  
endfunction
