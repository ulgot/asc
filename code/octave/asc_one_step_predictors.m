%%%
%    -- Funkcja [xbar, r] = asc_one_step_predictors(x, phi, theta)
%         oblicza jednokrokowe predyktory {xbar} procesu {x} na podstawie 
%         procesu ARMA(p,q), z parametrami \phi i \theta
%
%         przyjmuje jako agrumenty
%         x      - szereg czasowy (niepusty wektor danych)
%         phi    - wektor wielomianu AR
%         theta  - wektor wielomianu MA
%         
%         Zwraca
%         xbar - wektor predyktorów
%         r    - wektor błędów predykcji 
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: Dec 2010

function [xbar, r] = asc_one_step_predictors(x, phi, theta)

    if ( isempty (x) || (!isvector(x)) ) 
    error ("OSP: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif

  %dlugosc wektora
  lx = length(x);
  q  = length(theta);
  p  = length(phi);
  
  if ( !isvector(phi) || isempty(phi) || !isvector(theta) || isempty(theta))
    error ("OSP: drugi i trzeci argument musi byc niepustym wektorem\n");
    return;
  endif
  
  xbar = zeros(1, lx);
  r    = zeros(1, lx);
  %m    = q;
  m    = max(q,p); %TO DO & RETHINK!
  
  if q == m
    vec = theta;
  else
    vec = phi;
  endif
  
  % m pierwszych predyktorów
  for i = 2 : m
    suma = 0;
    for j = 1 : i
      suma += vec(j) * (x(i+1-j) - xbar(i+1-j));
    endfor
    xbar(i) = suma;
  endfor
  
  % lx - m - 1 kolejnych predyktorów
  for i = m + 1: lx
    suma_phi   = 0;
    for k = 1 : p
      suma_phi += phi(k) * x(i + 1 - k);
    endfor
    suma_theta = 0;
    for j = 1 : q
      suma_theta += theta(j) * (x(i+1-j) - xbar(i+1-j));
    endfor 
    xbar(i) = suma_phi + suma_theta;
  endfor
  
  % wektor błędów predyktorów
  r            = [];
  theta_r      = zeros(1, lx);
  theta_r(1:q) = theta;
  
  r(1) = 1;
  for n = 2 : lx
    suma_r = 0;
    for j = 1 : n - 1
      suma_r += power(theta_r(n-j),2) * r(j);
    endfor
    r(n) = 1 - suma_r;
  endfor
  
endfunction
