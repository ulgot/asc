%%%
%    -- Funkcja [z] = asc_moving_average(x, c = 7)
%         oblicza c-punktową średnią kroczącą centralną
%
%         Przyjmuje jako argumenty
%         x - szereg czasowy (niepusty wektor danych)
%         c - ilość punktów do obliczenia średniej
%             c < length(x) [= 7]
%             c musi być nieparzyste
%
%         Zwraca
%         z - krzywą dopasowaną do danych {x}
%         uwaga: length(z) = length(x) - c + 1
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: March 2010

function [z] = asc_moving_average(x, c = 7)
  
  if ( isempty (x) || (!isvector(x)) ) 
    error ("movingAverage: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif
  
  if (mod(c,2) == 0 || c < 1 || c > length(x))
    error("s=%d, Podaj nieparzyste s >= 1\n",c);
    return;
  endif

 q = (c-1)/2;
 for i = (q+1):(length(x)-q)
   z(i-q) = mean(x(i-q:i+q));
 endfor

endfunction


