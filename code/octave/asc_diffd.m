%%%
%    -- Funkcja [m, Y] = asc_diffd(x [, d = -1, accuracy = 0.001, maxdeg = 5])
%         oblicza wektor reszt szeregu czasowego wykazujacego 
%         sezonowość stosując metodę różnicowania z przesunięciem d
%         do danych stacjonarnych
%
%         przyjmuje jako agrumenty
%         x - szereg czasowy (niepusty wektor danych)
%         d - opcjonalnie: okresowość danych, jezeli nie podane
%               program sam stara się oszacować okresowość
%               !!! jeszcze nie działa !!!
%         accuracy - opcjonalnie; dokładność średniej wektora reszt, 
%               decyduje o stopniu różnicowania szeregu z usuniętą 
%               sezonowością [= 0.001]
%         maxdeg - opcjonalnie; maksymalna wartość stopnia różnicowania
%               szeregu z usuniętą sezonowością [= 5]
%
%         zwraca 
%         m - wektor danych po usunięciu sezonowości
%         Y - wektor reszt
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: March 2010

function [m, Y] = asc_diffd(x, d, accuracy = 0.001, maxdeg = 5)

  if ( isempty (x) || (!isvector(x)) ) 
    error ("asc_diffd: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif
  
  if ( d == -1 )
    error("asc_diffd: Jeszcze nie zakodowane...");
  elseif (d == 0)
    error("asc_diffd: d > 0, podałeś d = %d", d);
    return;
  endif  
   
  %%%
  % wektor trendu
  m = x(d : length(x)) - x(1 : length(x)-d+1);
  
  %%%
  % wektor reszt - obliczany przez różnicowanie, do momentu, gdy
  % osiągniemy żądaną dokładność na zerową średnią reszt
  for deg = 1 : maxdeg
    Y = asc_diff(m,deg);
    if mean(Y) < accuracy
      break;
    endif
  endfor 
  
endfunction
