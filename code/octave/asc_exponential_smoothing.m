%%%
%    -- Funkcja [m] = asc_exponential_smoothing(x, a = 0.5)
%         wygładza szereg czasowy
%
%         Przyjmuje jako agrumenty
%         x - szereg czasowy (niepusty wektor danych)
%         a - parametr wygładzania wykładniczego
%             0 < a < 1 [= 0.5]
%
%         Zwraca
%         m - wygładzony wektor procesu {x}
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: Dec 2010

function [m] = asc_exponential_smoothing(x, a = 0.5)
  
  if ( isempty (x) || (!isvector(x)) ) 
    error ("movingAverage: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif
  
  if (a < 0 || a > 1)
    error("a=%d, Podaj 0 < a < 1\n",a);
    return;
  endif

  m(1) = x(1);
  for t = 2 : length(x)
     m(t) = a * x(t) + (1 - a) * m(t - 1);
  endfor
  
endfunction

