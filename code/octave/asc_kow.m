%%%
%    -- Funkcja [z] = asc_kow(x, stopien = 0)
%         oblicza funkcję autokowariancji próby
%         procesu stacjonarnego X, na podstawie realizacji {x}
%
%         przyjmuje jako argumenty
%         x - szereg czasowy (niepusty wektor danych)
%         stopien - stopień korelacji [= 0]
%
%         Zwraca
%         z - wektor autokowariancji \gamma(h)
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: Mai 2010

function [z] = asc_kow(x, stopien = 0)
    
  if ( isempty (x) || (!isvector(x)) ) 
    error ("kow: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif

  %funkcja autokowariancji jest parzysta
  h = abs(stopien);

  %dlugosc wektora
  lx = length(x);
 
  if ( !isnumeric(stopien) || h >= lx)
    error ("kow: drugi argument musi byc liczba naturalna < dim(x)");
    return;
  endif
 
  %srednia
  srednia = mean(x);

  %suma
  korelacje = 0;
  for j=1:(lx-h)
    korelacje += x(j+h) * x(j);
  endfor

  z = (
    korelacje - 
    (sum(x(1:lx-h)) + sum(x(1+h:lx))) * srednia + 
    power(srednia,2) * (lx-h)
    ) / lx;

endfunction
