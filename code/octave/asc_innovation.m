%%%%%%
%    -- Funkcja [theta,v] = asc_innovation(x, q)
%         funkcja dopasowywuje proces MA(q) do 
%         procesu stacjonarnego X, na podstawie realizacji {x}
%         korzystając z algorytmu innowacyjnego
%
%         Przyjmuje jako argumenty
%         x - stacjnarny szereg czasowy (niepusty wektor danych)
%         q - rząd wielomianu MA, q > 0
%
%         Zwraca
%         theta - wektor współczynników modelu MA(q)
%         v     - wariancję procesu MA(q)
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: Mai 2010

function [theta,v] = asc_innovation(x, qIn)
    
  if ( isempty (x) || (!isvector(x)) ) 
    error ("I: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif

  %dlugosc wektora
  lx = length(x);

  if ( !isnumeric(qIn) || qIn < 1)
    error ("I: drugi argument musi byc liczba naturalna q > 0\n");
    return;
  endif

  %%%
  % algorytm innowacyjny

  %%%
  % pracujemy na wektorach
  w    = [];
  teta = [];

  %%%
  % wartości startowe
  % dla v(1) i theta(1,1)
  v_zero = asc_kow(x,0);
  w 	   = v_zero;
  teta   = asc_kor(x,1);

  %%%
  % właściwy algorytm innowacyjny
  for q = 1 : qIn   % dla rzedu = MA(1)...MA(q)

    %%%
    % kolejne wartości \theta_{q,q-k}
    for k=0:q-1
      suma = 0;
      for j=0:k-1
      	if k > j
	        suma += teta(q,q-j) * teta(k,k-j) * w(j+1);
      	endif
      endfor
      teta(q,q-k) = (asc_kow(x,q-k) - suma) / w(k+1);

      %%%
      % kolejne wartości v_q
      suma = 0;
      for j=0:q-1
      	if j == 0
	        fau = v_zero; 
	      else
	        fau = w(j);
      	endif
        if q > j
	         suma = power(teta(q,q-j),2) * fau;
        endif
      endfor
      w(q) = asc_kow(x,0) - suma;
    end %for k=0:q-1

  end %for q=1:qIn

  theta = teta;
  v     = w;

endfunction
