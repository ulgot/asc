%%%%%%
%    -- Funkcja [z] = asc_fakc(x, stopien = 1)
%         oblicza funkcję autokorelacji częściowej próby
%         procesu stacjonarnego X, na podstawie realizacji {x}
%
%         Przyjmuje jako argumenty
%         x - szereg czasowy (niepusty wektor danych)
%         stopien - stopień korelacji [= 1]
%
%         Zwraca
%         z - wektor autokorelacji częściowej próby, \alpha(h)
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: Mai 2010

function [z] = asc_fakc(x, stopien = 1)
  
  if ( isempty (x) || (!isvector(x)) ) 
    error ("autokor_czesc: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif

  %dlugosc wektora
  lx = length(x);

  %funkcja autorelacji jest parzysta
  h = abs(stopien);

  if ( !isnumeric(stopien) || h == 0 || h > lx)
    error ("autokor_czesc: drugi argument musi byc liczba naturalna 0 < |h| <= dim(wektor)\n");
    return;
  endif

  G = [];
  for i=0:h
    G(i+1) = asc_kow(x,i);
  endfor
  G = G';

  M = [];
  for i=0:h-1
    for j=0:h-1
      M (i+1,j+1) = G(abs(i-j)+1);
    endfor
  endfor
  %M

  %M * P = G => P = M^-1 * G
  P = inv(M) * G(2:length(G));

  % \alpha = P(h)
  z = P(length(P));

endfunction
