%%%
%  -- Funkcja: [z] = asc_spencer(x)
%       zwraca 15-punktową średnią kroczacą w/g Spencera
%       z wagami       
%       [74, 67, 46, 21, 3, -5, -6, -3] / 320;
%       
%       x - 1D wektor danych wejściowych
%           [length(X) >= 18]
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: March 2010

function [z] = asc_spencer(x)

  length_x = length(x);

  if ( isempty (x) || (!isvector(x)) || length_x < 18) 
    error ("\n\nasc_spencer:\npierwszy agrument musi byc niepustym \
    wektorem\no długości większej lub równej 18\npodano wektor od \
    długości: %d \n\n",length(x));
    return;
  endif

  %%%
  % wagi spencera
  spencer = [74, 67, 46, 21, 3, -5, -6, -3] / 320;
  length_spencer = length(spencer);  

  z = zeros(1, length_x - 2 * length_spencer);
  for i = length_spencer + 1 : length_x - length_spencer - 1
    for j = -length_spencer + 1 : length_spencer - 1
      z(i - length_spencer) += spencer(abs(j) + 1) * x(i + j);
    endfor
  endfor
  
endfunction
