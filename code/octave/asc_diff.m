%%%
%    -- Funkcja [Y] = asc_diff(x, k = 2)
%         oblicza wektor reszt szeregu czasowego
%         stosując metodę różnicowania do danych stacjonarnych
%
%         Przyjmuje jako argumenty
%         x - szereg czasowy (niepusty wektor danych)
%         k - rząd różnicowania, jeżeli nie podany to k = 2
%
%         Zwraca
%         Y - wektor reszt
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: March 2010

function [Y] = asc_diff(x, k = 2)

  if ( isempty (x) || (!isvector(x)) ) 
    error ("asc_diff: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif
  
  if ( k < 1 )
    error("k > 0, podałeś k = %d", k);
    return
  endif
  
  for i = 1 : k
    Y = x(2 : length(x)) - x(1 : length(x)-1);
    x = Y;
  endfor
    
endfunction
