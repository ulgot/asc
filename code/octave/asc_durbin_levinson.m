%%%
%    -- Funkcja [phi, v] = asc_durbin_levinson(x, p)
%         dopasowywuje proces AR(p) do danego szeregu czasowego
%         metodą Durbina - Levinsona
%
%         Przyjmuje jako argumenty
%         x - stacjonarny szereg czasowy (niepusty wektor danych)
%         p - rząd wielomianu AR, p >= 0
%
%         Zwraca
%         phi - wektor parametrów procesu AR(p)
%         v   - wariancję procesu AR(p)
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: Mai 2010

function [phi, v] = asc_durbin_levinson(x, p)
    
  if ( isempty (x) || (!isvector(x)) ) 
    error ("DL: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif

  %dlugosc wektora
  lx = length(x);

  if ( !isnumeric(p) || p < 1)
    error ("DL: drugi argument musi byc liczba naturalna 0 < |h| <= dim(wektor)\n");
    return;
  endif

  %%%
  % algorytm Durbina - Levinsona
  
  fi = [];
  v  = [];
  
  % krok 0
  if (p == 1)
    p;
    phi = asc_kor(x,1);
    v   = asc_kow(x,0) * (1.0 - power(phi,2.0));
  else
    [fi, w] = asc_durbin_levinson(x, p - 1);
    p;

    suma = 0.0;
    for j=1:p-1
      suma += fi(p-1,j) * asc_kow(x,p-j);
    endfor
    
    fi(p, p) = (asc_kow(x,p) - suma) / w(p-1);

    for m=1:p-1
      fi(p,m) = fi(p-1,m) - fi(p,p)*fi(p-1,p-m);
    endfor

    w(p)     = w(p-1) * (1.0 - power(fi(p,p),2.0));

    phi = fi;
    v   = w;
  end

endfunction
  
