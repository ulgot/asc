%%%%%%
%    -- Funkcja [z] = asc_kor(x, stopien = 0)
%         oblicza funkcję autokorelacji próby
%         procesu stacjonarnego X, na podstawie realizacji {x}
%
%         przyjmuje jako argumenty
%         x - szereg czasowy (niepusty wektor danych)
%         stopien - stopień korelacji [= 0]
%
%         Zwaraca
%         z - wektor autokorelacji \rho(h)
%%%

## This file is part of ASC
## Author: Lukasz Machura <lukasz.machura@us.edu.pl>
## Created: Mai 2010

function [z] = asc_kor(x, stopien = 0)

  if ( isempty (x) || (!isvector(x)) ) 
    error ("kor: pierwszy agrument musi byc niepustym wektorem");
    return;
  endif

  if ( !isnumeric(stopien) )
    error ("kor: drugi argument musi byc liczba naturalna");
    return;
  endif

  %funkcja autorelacji jest parzysta
  h = abs(stopien);

  if ( h == 0)
    z =  1;
  else
    z = asc_kow(x,h) / asc_kow(x,0);
  endif

endfunction
