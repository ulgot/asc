import numpy
import matplotlib.pyplot


class Histogram(object):

    def __init__(self, data, bins=10, normed=True, range=None):

        self._values, self._bins_edges = numpy.histogram(
            data,
            bins,
            range,
            False,
            None,
            normed)

    @property
    def values(self):
        return self._values

    @property
    def bins_edges(self):
        return self._bins_edges

    def plot(self):
        matplotlib.pyplot.bar(self.bins_edges[:-1],
                              self.values,
                              width=self.bins_edges[1]-self.bins_edges[0])
        matplotlib.pyplot.xticks(self.bins_edges)
        matplotlib.pyplot.xlim(self.bins_edges[0], self.bins_edges[-1])
