import numpy
from abstract_model import Model
from asc.core.time_series import TimeSeries


class HoltModel(Model):

    def __init__(self, data, alpha, beta):

        self._Model_forecast_offset = 1
        super(HoltModel, self).__init__(
            data,
            {"alpha": alpha, "beta": beta})

    @property
    def alpha(self):
        return self.__alpha

    @alpha.setter
    def alpha(self, value):
        if 0 <= value <= 1:
            self.__alpha = value
        else:
            raise ValueError("alpha must be a number from the interval [0,1].")

    @property
    def beta(self):
        return self.__beta

    @beta.setter
    def beta(self, value):
        if 0 <= value <= 1:
            self.__beta = value
        else:
            raise ValueError("beta must be a number from the interval [0,1].")

    @property
    def estimated_series(self):
        return self.__estimated_series

    @property
    def initial_condition(self):
        return self.__initial_condition

    @initial_condition.setter
    def initial_condition(self, value):
        if value in ("linear", "data"):
            self.__initial_condition = value
        else:
            raise ValueError("Unknown initial condition type."
                             "Supported types are: linear, data")

    @property
    def forecast_offset(self):
        return 1

    def get_parameter(self, param):
        if param == "alpha":
            return self.alpha
        elif param == "beta":
            return self.beta
        else:
            raise ValueError("alpha must be a number from the interval [0,1].")

    def set_parameter(self, param, value):
        if param == "alpha":
            self.alpha = value
        elif param == "beta":
            self.beta = value
        else:
            raise ValueError("alpha must be a number from the interval [0,1].")

    def recalculate_model(self):

        sample_size = len(self.data)

        f_t = numpy.zeros(sample_size)
        s_t = numpy.zeros(sample_size)

        if self.initial_condition == "data":
            f_t[0] = self.data[0]
            s_t[0] = self.data[1] - self.data[0]
        elif self.initial_condition == "linear":
            f_t[0], s_t[0] = numpy.linalg.lstsq(
                numpy.array([numpy.arange(sample_size),
                             numpy.ones(
                                 sample_size)]).T,
                self.data)[0]

        a = 1 - self.alpha
        b = 1 - self.beta

        for k in range(sample_size - 1):
            f_t[k + 1] = self.alpha * self[k + 1] + a * (f_t[k] + s_t[k])
            s_t[k + 1] = self.beta * (f_t[k + 1] - f_t[k]) + b * s_t[k]

        self.components = {}

        if self.data.time_data is not None:
            t = self.data.time_data[1:]
        else:
            t = None

        self.components["smoothened"] = TimeSeries(f_t, t)
        self.components["trend"] = TimeSeries(s_t, t)
        self.components["residuals"] = TimeSeries(self.data[1:] - f_t, t)
