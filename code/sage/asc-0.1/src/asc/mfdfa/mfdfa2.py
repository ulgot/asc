def mfdfa2(signal, m=1, scales=[]):
    """
    ***
    * based on
    * 1. "Introduction to multifractal detrended fluctuation analysis in Matlab", 
    *  	E. A. F. Ihlen, Front. Physio. 3, 141 (2012)
    * 2. "Multifractal detrended fluctuation analysis of nonstationary time series",
    *	J. W. Kantelhardt, S. A. Zschiegner, E. Koscielny-Bundec, S. Havlin, 
    *	A. Bunde and H. E. Stanley, Phys. A 316, 87 (2002)
    ***

    Niebrzydki kod w numpy przepisany praktycznie 1 do 1 z Matlaba. 
    TBA: dac helpa i pomyslec nad spythonieniem kodu...
    """
    
    import numpy

    signal = numpy.array(signal)

    #ilosc danych
    ldn = len(signal)    
    X = numpy.cumsum(signal - numpy.mean(signal))
    
    #automatyczne skale (min okno ma 16 punktow)
    if scales == []:    
        scales = [2**i for i in range(4,int(numpy.log2(ldn/10.)) + 2)]
    
    #predefiniowane skale do lokalnego wygladzania danych
    scale_small = [7, 9, 11, 13, 15, 17]
    halfmax = int(numpy.max(scale_small)/2.)
    idx_start, idx_stop = halfmax, len(X)-halfmax
    
    RMS_ns = []
    for ns in scale_small:
        RMS_scale_small = []
        halfseg = int(ns/2.)
        for v in range(idx_start,idx_stop):
            T_idx_start, T_idx_stop = v-halfseg, v+halfseg
            C = numpy.polyfit(range(T_idx_start,T_idx_stop), X[T_idx_start:T_idx_stop],m)
            fit = numpy.poly1d(C)
            _b = numpy.sqrt(numpy.mean((X[T_idx_start:T_idx_stop] - fit(range(T_idx_start,T_idx_stop)))**2))
            RMS_scale_small.append(_b)
        RMS_ns.append(RMS_scale_small)
    RMS_ns = dict(zip(scale_small,RMS_ns))
    
    F_q0 = []
    for ss in scales:
        segments = int(ldn/ss)
        
	_b = []
        for v in range(segments):
            idx_p, idx_k = v*ss, (v+1)*ss
            C = numpy.polyfit(range(idx_p,idx_k), X[idx_p:idx_k],m)
            fit = numpy.poly1d(C)
            _b.append(numpy.sqrt(numpy.mean((X[idx_p:idx_k] - fit(range(idx_p,idx_k)))**2)))
        _b = numpy.array(_b)

        F_q0.append(numpy.exp(0.5*numpy.mean(numpy.log(_b**2))))
    dF_q0 = dict(zip(scales,F_q0))
    
    C = numpy.polyfit(numpy.log2(numpy.array(scales)),numpy.log2(numpy.array(F_q0)),1)
    Regfit = numpy.poly1d(C)
    Hq0 = C[0]
    
    Ht = []
    for ns in scale_small: 
        RMSt = numpy.log2(RMS_ns[ns][idx_start:idx_stop])
        _resRMS = Regfit(numpy.log2(ns)) - RMSt
        _lns = numpy.log2(ldn) - numpy.log2(ns)
        Ht.append(_resRMS/_lns + Hq0)
    
    _htrow = numpy.array(Ht).reshape(-1)
    _bins = int(numpy.round(numpy.sqrt(len(_htrow))))
    _freq, Htbins = numpy.histogram(_htrow,_bins)

    # here is some nasty hack: for the logarithm in Dh we can't have 0 counts 
    #   therefore we put 0.1 instead of 0
    #	TODO: just get rid of zeros - than Htbins would need the redefinition
    Ph = numpy.array([numpy.float(i) if i!=0 else 0.1 for i in _freq])/_freq.sum()
    Ph_norm = Ph/numpy.max(Ph)
    Dh = 1.0 - (numpy.log(Ph_norm)/-numpy.log(numpy.mean(scales)))
    
    return {
	'Dh': Dh.tolist(),
	'Ph': Ph.tolist(),
	'Ht': Ht,
	'Htbins': Htbins.tolist(),
	'scales': scales,
	'm': m,
	'data': signal.tolist(),
	}

    #TODO: think of dictionaries for different m's like in mfdfa1
