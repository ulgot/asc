def dataplot_qRMS(dicto, m=1, skala=16, q=1, *arg, **kwarg):
    dx = range(len(dicto['data']))
    dy = []
    for i in dicto['qRMS'][m][skala][q]:
        _qrms = [i for j in range(skala)]
        dy += _qrms
    return zip(dx,dy)  

def get_spectrum(dicto, m=None, full=False, with_q_zero=True):
  """get_spectrum(dictionary, m=1, full=False)"""

  if m == None:
    m == dicto['m'][0]

  _q = dicto['q'][:-1]
  _dx = [dicto['hq'][m][q] for q in _q]
  _sdx = sorted(_dx)
  _dxy = dict([(dicto['hq'][m][q],dicto['Dq'][m][q]) for q in _q])
  _zd  = zip(_sdx,[_dxy[i] for i in _sdx])
  
  if full:
    deltax = _q[1] - _q[0]
    tenpercent = len(_q)/10 + 1
    
    #rare events: q -> infinity
    qrange = _q[-tenpercent:]
    rare_events = [(dicto['hq'][m][q],dicto['Dq'][m][q]) for q in qrange]

    #smooth part : q -> infinity
    qrange = _q[:tenpercent]
    smooth_part = [(dicto['hq'][m][q],dicto['Dq'][m][q]) for q in qrange]

    hurst = [(dicto['hq'][m][q],dicto['Dq'][m][q]) for q in _q if 2.0-deltax < q < 2.0+deltax][0]
    spectrum_maximum = [(dicto['hq'][m][q],dicto['Dq'][m][q]) for q in _q if -deltax < q < deltax][0]
    half_width = abs(spectrum_maximum[0] - hurst[0])

    ret = {'spectrum':_zd, 'rare':rare_events, 'smooth':smooth_part, 
	'max':spectrum_maximum, 'hurst':hurst, 'width':half_width}
  else:
    ret = {'spectrum':_zd}

  return ret
