# -*- coding: utf-8 -*-

def dfa(data, polyorder=[1], scales=[], shuffle=False, raw_data=False, ndiff=None, walking_window=None, quiet=True):
    """
    Funkcja obliczajaca fluktuacje metoda DFA (detrended fluctuation analysis). Dodatkowo 
    zwracany jest wykladnik Hursta.
    http://en.wikipedia.org/wiki/Detrended_fluctuation_analysis

    INPUT:
      data    - dane w postaci listy lub numpy.array zawierajace badany szereg czasowy 
      	        (1D, zakladamy uporzadkowanie w czasie);
      polyorder - lista zawierajaca rzad (rzedy) wielomianow dopasowywanych do danych 
      		w danym oknie; domyslnie dopasowanie jest liniowe;
      scales  - lista zawierajaca wielkosci okien branych w analizie do obliczania 
      		wykladnika Hursta. Powinny zawierac potegi liczby 2;
		jezeli lista jest pusta (domyslnie) skale obliczane sa automatycznie;
      shuffle - bazujemy na danych oryginalnych, czy przetasowanych losowo; 
      		ma to na celu sprawdzenie, czy multifraktalne charakterystyki
		pochodza z korelacji dalekozasiegowych, czy z szerokiego
		rozkladu gestosci prawdopodobienstwa (jezeli multifraktalnosc 
		bierze sie z korelacji, to zniknie):
		dla pomieszanych danych;
      		True - miesza losowo dane przed obliczaniem charakterystyk,
		False - (domyslnie) pracuje na oryginalnych danych;
      ndiff - roznicowanie danych, obliczanie przyrostow oryginalnych danych,
	      typowa technika osiagania stacjonarnosci z danych niestacjonarnych:
	      None - (domyslnie) pracuje na oryginalnych danych
	      1,2,3,... - stopien roznicowania
      raw_data - definiuje dane ktore poddajemy analizie
                 True  - oblicza charakterystyki dla danych oryginalnych (po odjeciu sredniej)
                 False - (domyslnie) oblicza charakterystyki dla danych scalkowanych (cumsum)
      walking_window - definiuje nalozenie sie segmentow podczas obliczania Fq na siebie
                       None - (domyslnie) segmenty rozdzielne
		       1,2,3,... - nakladajace sie segmenty, jest to krok z jakim segment ma 
		       podrozowac po danych

    OUTPUT:

      Slownik
      {'Fq':Fq, 'RMS':RMS, 'qRMS':qRMS, 'm':polyorder, 
      	'scales':scales, 'q':qorder, 'data':data, 'Hq':Hq,
	'tq':tq, 'hq':hq, 'Dq':Dq}

      dane wejsciowe:
      m      - lista rzedow wielomianow uzytych do dopasowania polyfit-em: dict['m']
      scales - lista wielkosci okien (skal) uzytych w analizie w postaci wykladnika
	       potegi liczby 2, skala 16 <= s <= max(len(data)): dict['scales']
      data   - lista trzymajaca dane: dict['data']

      dane z analizy:
      RMS - slownik; trzyma lokalny trend dopasowany wielomianem 'm'-tego rzedu dla 
      	    danej skali 's' dla calosci danych (obcietych do wielokrotnosci dlugosci 
	    okna):  dict['RMS'][m][s]
      H   - wykladnik Hursta: dict['H'][m]
 
    EXAMPLES:
      zakladamy, ze szereg mamy w liscie 'dane' (moze ty byc lista pythonowa lub 
      ndarray numpy)

      sage: dict_dfa = dfa(dane)
        zwroci nam slownik wielkosci dla m = 1, wielkosci okien 
	scales=[16..floor(log2(len(dane)/10))] 
      
      sage: _fit = [1,2,3]
      sage: _sca = [2**i for i in range(4,max_range)]
      sage: dict_dfa = dfa(dane, polyorder=_fit, scales=_sca)
        zwroci nam slownik wielkosci dla m = [1,2,3] (liniowe, kwadratowe i szescianowe 
	dopasowanie), wielkosci okien scales=[16,32,64,128,...,MAX2] (od 16 az do okna 
	o szerokosci mieszczacej sie tylko raz w obrebie danych)
      
      sage: dict_dfa = dfa(dane, polyorder=[2], scales=[32,64,128])
        zwroci slownik dla wszystkich podanych wielkosci 
	"""
    
    import numpy

    # numpy.array
    X = numpy.array(data)

    # roznicowanie danych (do stacjonarnosci)
    if ndiff != None:
      X = numpy.diff(X, n=ndiff)
    
    # charakterystyki dla pomieszanych danych
    if shuffle:
      numpy.random.shuffle(X)

    # raw of integrated data
    if raw_data:
      X = X - numpy.mean(X)
    else:
      X = numpy.cumsum(X - numpy.mean(X))


    #automatic scales
    if scales == []:
      scales = [2**i for i in range(4,int(numpy.floor(numpy.log2(len(data)/10))) + 2)]

    F_m = []
    H_m = []
    for m in polyorder:
        """rzad wielomianu do dopasowania"""
        
        F_scale = []
        for ns in scales:
            
	    if walking_window == None:
	      step = ns
              segments = int(numpy.floor(len(X)/ns))
	    else:
	      assert(0 < walking_window < ns)
	      step = walking_window
	      segments = int(numpy.floor(len(X)/step)) - ns - 1
            
	    if not quiet:
	      print "len(X) %d, step %d, segments %d, step*segments %d"%(len(X), step, segments, step*segments)
            
            F_dla_skali = []
            for v in range(segments):
		idx_start = v * step
		idx_stop  = idx_start + ns

                C = numpy.polyfit(range(idx_start,idx_stop), X[idx_start:idx_stop],m)
                fit = numpy.poly1d(C)
                _b = numpy.mean((X[idx_start:idx_stop] - fit(range(idx_start,idx_stop)))**2)
  		# ***
                F_dla_skali.append(_b)
            
            F_scale.append(numpy.sqrt(numpy.mean(F_dla_skali)))
        
        F_dict_scale = dict(zip(scales,F_scale))

        # dfa method for calculating Hurst exponent
	C = numpy.polyfit(numpy.log2(numpy.array(scales)),numpy.log2(numpy.array(F_scale)),1)

        F_m.append(F_dict_scale)
        H_m.append(C[0])
    
    H = dict(zip(polyorder,H_m))
    F = dict(zip(polyorder,F_m))
    
           
    return {
	'data':data.tolist(),
	'BW':X.tolist(),
	'm':polyorder,
	'scales':scales,
	'F':F,
	'H':H,
	}
