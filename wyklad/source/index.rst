.. SPHINX_START_PROJECT - Skrypt documentation master file, created by
   sphinx-quickstart on Fri Mar  8 15:41:59 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=================================
Analiza szeregów czasowych z SAGE
=================================

:Autor: 
        Łukasz Machura
:Wersja: 
        2.0 3/2014
        sage_1.0 3/2014
:Pobierz:
        :download:`podręcznik (v1.00, PDF) <pdf/ASC_v_sage1.00.pdf>`

  * Celem zajęć jest wprowadzenie do analizy szeregów czasowych w oparciu o system SAGE

  * przedmiot do wyboru na 1 roku studiów II stopnia (magisterskich) na kierunku Ekonofizyka UŚ


Wymagania

  * podstawowa znajomość języka Python lub środowiska SAGE

  * podstawowa znajomość metod numerycznych        


Spis rzeczy
===========

.. toctree::
   :maxdepth: 1

   01_wstep
